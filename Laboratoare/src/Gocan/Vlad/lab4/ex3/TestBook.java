package Gocan.Vlad.lab4.ex3;
import Gocan.Vlad.lab4.ex2.Author;

public class TestBook {

    public static void main(String[] args)
    {


        Author a1 = new Author("Gocan Vlad", "gva20072007@yahoo.com", 'm');
        Author a2 = new Author("M F", "xyz@yahoo.com", 'f');
        Author a3 = new Author("x","y",'z');

        System.out.println(a1.getName());
        System.out.println(a1.getGender());
        System.out.println(a1.getEmail());
        System.out.println();
        System.out.println(a3.getGender());

        Book b1 = new Book("balada",a1,100);
        Book b2 = new Book("drama",a2,299,50);

        System.out.println(b1.getAuthor());
        System.out.println(b1.getName());
        System.out.println(b1.getPrice());
        System.out.println();

        System.out.println(b2.getAuthor());
        System.out.println(b2.getName());
        System.out.println(b2.getPrice());
        System.out.println(b2.getQtyInStock());
        System.out.println();

        b2.setPrice(50);
        b1.setQtyInStock(100);
        System.out.println("Cartii b1 i s-a schimbat pretul in "+b1.getPrice());
        System.out.println(b2.getPrice());
        System.out.println(b2.getQtyInStock());


        System.out.println(b1.toString());


    }
}