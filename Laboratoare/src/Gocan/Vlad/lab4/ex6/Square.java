package Gocan.Vlad.lab4.ex6;

public class Square extends Rectangle  {


    public Square() {
    }

    public Square(double side) {
        super(side,side);
    }

    public Square(double side, String color, boolean filled) {
        super(side,side,color,filled);
    }

    public double getSide(){
        return this.getLength();
    }

    public void setSide(double side) {

        this.setLength(side);
        this.setWidth(side);
    }

    public void setWidth(double side1) {

        super.setWidth(side1);
        super.setLength(side1);
    }

    public void setLength(double side2) {

        super.setLength(side2);
        super.setWidth(side2);
    }

    public String toString(){
        return ("A Square with side = "+this.getLength()+" which is a subclass of ");
    }
}