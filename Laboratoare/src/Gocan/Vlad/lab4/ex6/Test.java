package Gocan.Vlad.lab4.ex6;

public class Test {

    public static void main(String[] args) {
        Shape s = new Shape("red", true);
        System.out.println(s.toString());

        Circle c = new Circle(2,"green",false);
        System.out.println(c.isFilled());

        Square sq = new Square(2,"blue",true);
        sq.getLength();

        System.out.println(sq.getSide());
    }
}