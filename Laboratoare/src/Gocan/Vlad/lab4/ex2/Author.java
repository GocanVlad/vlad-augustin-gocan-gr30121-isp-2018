package Gocan.Vlad.lab4.ex2;


public class Author {


    private String name, email;
    private char gender;

    public Author(String name, String email, char gender)
    {
        this.name=name;
        this.email=email;
        this.gender=gender;
    }

    public String getEmail() {
        return email;
    }

    public String getName()
    {
        return name;
    }

    public char getGender()
    {
        if(Character.toLowerCase(gender)=='m' || Character.toLowerCase(gender)=='f')

            return gender;
        else
        {
            System.out.println("Gender not accepted");
            return 0;
        }

    }

    public  void setEmail(String email)
    {
        this.email=email;

    }



    public String  toString()
    {
        return (this.name+" ("+this.gender+") at "+this.email);
    }

}
