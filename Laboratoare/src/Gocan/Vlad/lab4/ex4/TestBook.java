package Gocan.Vlad.lab4.ex4;

import Gocan.Vlad.lab4.ex2.Author;

public class TestBook {

    public static void main(String[] args) {

        Author[] authors = new Author[3];
        authors[0] = new Author("Gocan Vlad","gva20072007@yahoo.com",'m');
        authors[1]= new Author("xyz","xyz@yahoo.com",'f');
        authors[2]= new Author("X Y","y@yahoo.com",'z');

        Book book = new Book("drama",authors,50);
        book.printAuthors();
        book.toString();


    }
}
