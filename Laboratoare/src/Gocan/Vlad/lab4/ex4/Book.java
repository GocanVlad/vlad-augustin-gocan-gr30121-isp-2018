package Gocan.Vlad.lab4.ex4;

import Gocan.Vlad.lab4.ex2.Author;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock=0;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors=authors;
        this.price = price;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors=authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public void printAuthors(){
        for(int i=0;i<this.authors.length; i++)
            System.out.println("Author name "+authors[i].getName());
    }

    public String toString(){
        return ("this book " + this.name+ " was written by "+this.authors.length +" different authors");
    }


}
