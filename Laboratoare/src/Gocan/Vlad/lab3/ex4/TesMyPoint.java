package Gocan.Vlad.lab3.ex4;

public class TesMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint();
        p1.setX(5);
        p1.setY(3);
        System.out.println(p1.getX() + " " + p1.getY());

        p1.setXY(3, 4);
        System.out.println(p1.getX() + " " + p1.getY());

        p2.setX(6);
        p2.setY(2);

        System.out.println("distance " + p1.distance(5, 3));
        System.out.println("distance " + p1.distance(p2));


    }
}
