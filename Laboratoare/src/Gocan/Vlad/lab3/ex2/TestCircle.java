package Gocan.Vlad.lab3.ex2;

public class TestCircle {
    public static void main(String[] args)
    {
        Circle c1=new Circle();
        Circle c2 = new Circle(4.0);
        Circle c3 = new Circle(5.0, "blue");
        System.out.println(c1.getRadius());
        System.out.println(c1.getArea());
        System.out.println(c1.getColor());
        System.out.println(c2.getRadius());
        System.out.println(c2.getArea());
        System.out.println(c3.getRadius());
        System.out.println(c3.getArea());
        System.out.println(c3.getColor());

    }
}
