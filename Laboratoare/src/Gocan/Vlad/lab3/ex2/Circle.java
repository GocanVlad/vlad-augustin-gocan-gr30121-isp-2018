package Gocan.Vlad.lab3.ex2;

public class Circle {
    private double radius;
    private String color;

    public Circle()
    {
        this.color="red";
        this.radius=1.0;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public String getColor()
    {
        return color;
    }
}
