package Gocan.Vlad.lab5.ex4;

import Gocan.Vlad.lab5.ex3.LightSensor;
import Gocan.Vlad.lab5.ex3.TemperatureSensor;
import Gocan.Vlad.lab5.ex3.Sensor;

import java.util.Timer;
import java.util.TimerTask;

public class Controller {

    private static int ct=0;
    public Sensor lightSensor;
    public Sensor tempSensor;
    private static Controller controller;

    private Controller()
    {
        lightSensor = new LightSensor();
        tempSensor = new TemperatureSensor();
    }

    public static Controller getInstance()
    {
        if(controller==null)
            controller = new Controller();

        return controller;
    }

    public void control()
    {
        TemperatureSensor t = new TemperatureSensor();
        LightSensor l = new LightSensor();
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {

            public void run() {
                ct++;
                if (ct < 21) {
                    System.out.println("Temparature: " + t.readValue());
                    System.out.println("Light: " + l.readValue());
                    System.out.println("Seconds: "+ct);
                    System.out.println();
                }
                else {
                    System.out.println("Stop");
                    timer.cancel();
                }



            }

        };
        timer.schedule(task, 0, 1000);
    }
}
