package Gocan.Vlad.lab5.ex1;

public class Test {
    public static void main(String[] args) {
        Shape c1 = new Circle(10.0,"red",true);
        Shape c2 = new Circle(5);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        c1.toString();
        c2.toString();

        System.out.println();

        Shape r1 = new Rectangle(10.0, 20.0, "red", true);
        System.out.println(r1.getArea());
        System.out.println(r1.getPerimeter());
        r1.toString();

        System.out.println();

        Shape s1 = new Square(5.0, "magenda", false);
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
        s1.toString();

        System.out.println();

        Shape s2 = new Square();
        ((Square) s2).setLength(1.0);
        ((Square) s2).setWidth(1.0);
        System.out.println(s2.getArea());
        System.out.println(s2.getPerimeter());
    }
}
