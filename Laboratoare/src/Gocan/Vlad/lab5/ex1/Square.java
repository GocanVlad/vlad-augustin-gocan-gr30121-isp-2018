package Gocan.Vlad.lab5.ex1;

public class Square extends Rectangle {

    public Square() {
    }

    public Square(double side) {
        super(side,side);
    }

    public Square(double side, String color, boolean filled) {
        super(side,side,color,filled);
    }

    public double getSide(){
        return this.getLength();
    }

    public void setSide(double side) {

        this.setLength(side);
        this.setWidth(side);
    }

    public void setWidth(double side) {

        super.setWidth(side);
        super.setLength(side);
    }

    public void setLength(double side) {

        super.setLength(side);
        super.setWidth(side);
    }

    public String toString(){
        return ("A Square with side = "+this.getLength()+" which is a subclass of ");
    }

}
