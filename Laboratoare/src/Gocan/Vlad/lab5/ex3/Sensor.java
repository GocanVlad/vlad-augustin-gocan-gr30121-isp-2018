package Gocan.Vlad.lab5.ex3;

abstract public class Sensor
{
    private String location;

    abstract public int readValue();
    public String getLocation(){return this.location;}

}
