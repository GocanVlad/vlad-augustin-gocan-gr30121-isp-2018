package Gocan.Vlad.lab5.ex3;

import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    private static int ct = 0;


    public void control() {
        TemperatureSensor t = new TemperatureSensor();
        LightSensor l = new LightSensor();
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {

            public void run() {
                ct++;
                if (ct < 21) {
                    System.out.println("Temparature: " + t.readValue());
                    System.out.println("Light: " + l.readValue());
                    System.out.println("Seconds: "+ct);
                    System.out.println();
                }
                else {
                    System.out.println("Stop");
                    timer.cancel();
                }



            }

        };
        timer.schedule(task, 0, 1000);
    }
}
