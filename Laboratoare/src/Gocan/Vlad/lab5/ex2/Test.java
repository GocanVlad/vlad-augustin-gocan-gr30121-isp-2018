package Gocan.Vlad.lab5.ex2;

public class Test {

    public static void main(String[] args) {
        RealImage RealImg = new RealImage("Img.jpg");
        ProxyImage ProxyImg1 = new ProxyImage("Real");
        ProxyImage ProxyImg2 = new ProxyImage("Rotated");


        RealImg.display();
        ProxyImg1.display();
        System.out.println();
        ProxyImg2.display();
    }
}
