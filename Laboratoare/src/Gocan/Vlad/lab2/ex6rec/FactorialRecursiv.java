package Gocan.Vlad.lab2.ex6rec;

import java.util.Scanner;

public class FactorialRecursiv {
    public static int factorial(int n)
    {
        if (n > 1)
            return n * factorial(n - 1);
        else
            return 1;
    }

    public static void main(String[] args)
    {
        int n, fact = 1;
        Scanner in = new Scanner(System.in);
        System.out.print("Dati valoare lui n=  ");
        n = in.nextInt();
        if (n < 0)
        {
            System.out.print("NU EXISTA");
            return;
        }
        if (n == 0)
        {
            System.out.print("Factorialul este 1");
            return;
        }

        if (n >= 1)
            fact = factorial(n);


        System.out.print("Factorialul  este : " + fact);
    }
}