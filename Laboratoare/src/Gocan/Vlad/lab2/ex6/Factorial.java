package Gocan.Vlad.lab2.ex6;

import java.util.Scanner;

public class Factorial
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n;
        int produs = 1;
        System.out.println("Dati o valoare pentru n:");
        n = in.nextInt();
        if (n < 0)
        {
            System.out.println("NU EXISTA!");
            return;
        }
        if (n == 0)
        {
            System.out.println("n!:" + 1);
            return;
        }
        else
            for (int i = 1; i <= n; i++)
                produs *= i;
        System.out.println("n!:" + produs);

    }
}
