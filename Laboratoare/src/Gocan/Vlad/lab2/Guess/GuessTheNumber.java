package Gocan.Vlad.lab2.Guess;

import java.util.*;
import java.util.Scanner;
public class GuessTheNumber
{
    public static void main(String[] args)
    {
        Random r = new Random();
        int a = r.nextInt(10);
        int count=0;

        System.out.println();
        System.out.print("Guess the number! You have 3 chances!");

        Scanner in = new Scanner(System.in);

        while(count<=2)
        {
            int n0;
            System.out.println();
            System.out.println("The First Chance ");
            System.out.print("Introduce a number n1 =  ");
            n0 = in.nextInt();
            if(n0==a) {
                System.out.print("You guess the number");
                System.out.println();
                break;
            }
            if(n0<a)
                System.out.println("Wrong answer, your number is too low");
            if(n0>a)
                System.out.println("Wrong answer, your number is too high");

            count=count+1;

            System.out.println();

            int n1;
            System.out.print("The Second Chance");
            System.out.println();
            System.out.print("Introduce a number n2 =  ");
            n1 = in.nextInt();
            if(n1==a)
            {
                System.out.print("You guess the number");
                break;
            }
            if(n1<a)
                System.out.println("Wrong answer, your number is too low");
            if(n1>a)
                System.out.println("Wrong answer, your number is too high");

            count=count+1;

            System.out.println();
            int n2;
            System.out.print("The Last Chance");
            System.out.println();
            System.out.print("Introduce a number n3 =  ");
            n2= in.nextInt();
            if(n2==a)
            {
                System.out.print("You guess the number");
                break;
            }
            if(n2<a)
            {
                System.out.println("Wrong answer, your number is too low. You lost!");
                System.out.println("The given number was:" +a);
            }
            if(n2>a)
            {
                System.out.println("Wrong answer, your number is too high. You lost!");
                System.out.println("The given number was:" +a);
            }
            count=count+1;
        }
    }
}
