package Gocan.Vlad.lab2.ex2;

import java.util.Scanner;

public class PrintNumberInWord {

    public static void main(String[] args)
    {
        Scanner in= new Scanner(System.in);
        System.out.println("Dati o valoare intre 0 si 9");
        int x=in.nextInt();

        switch(x)
        {
            case 0: System.out.println("ZERO"); break;
            case 1: System.out.println("UNU"); break;
            case 2: System.out.println("DOI"); break;
            case 3: System.out.println("TREI"); break;
            case 4: System.out.println("PATRU"); break;
            case 5: System.out.println("CINCI"); break;
            case 6: System.out.println("SASE"); break;
            case 7: System.out.println("SAPTE"); break;
            case 8: System.out.println("OPT"); break;
            case 9: System.out.println("NOUA"); break;
            default:  System.out.println("INTRE 0 si 9!!"); break;
        }
    }
}
