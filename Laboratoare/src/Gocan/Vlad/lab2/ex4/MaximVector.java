package Gocan.Vlad.lab2.ex4;

import java.util.Scanner;

public class MaximVector {
    public static void main(String[] args)
    {
        int max;
        Scanner in= new Scanner(System.in);
        System.out.println("Marimea vectorului este:");
        int n=in.nextInt();
        int[] v= new int[n];
        for(int i=0;i<n;i++)
        {
            System.out.println("Elementul v["+i+"]:");
            v[i]=in.nextInt();
        }
        max=v[0];
        for(int i=0;i<n;i++)
            if(v[i]>max)
                max=v[i];
        System.out.println("Elementul maxim din vector este:" + max);
    }
}
