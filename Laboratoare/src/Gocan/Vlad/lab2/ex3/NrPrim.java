package Gocan.Vlad.lab2.ex3;

import java.util.Scanner;

public class NrPrim {

    static public void main(String[] args)
    {
        int ct = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("A:");
        int a = in.nextInt();
        System.out.println("B:");
        int b = in.nextInt();
        for (int i = a; i <= b; i++)
        {
            if(i!=1)
            {
                int prim = 1;
                int d = 2;
                while (d <= i / 2)
                {
                    if (i % d == 0 )
                        prim = 0;
                    d+=1;
                }
                if (prim == 1)
                {
                    System.out.print("Nr " + i + " este prim");
                    System.out.println();
                    ct++;
                }
            }
        }
        System.out.println();
        System.out.print("Au fost gasit " + ct + " nr prime");
    }
}
