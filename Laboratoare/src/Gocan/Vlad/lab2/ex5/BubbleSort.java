package Gocan.Vlad.lab2.ex5;

import java.util.Random;
import java.util.Scanner;

public class BubbleSort {


    public static void main(String[] args)
    {
        int i,n;
        Random r = new Random();
        Scanner in = new Scanner(System.in);
        System.out.print("Dimensiunea vectorului:");
        n=in.nextInt();

        System.out.println();
        int[] v = new int[n];

        for (i = 0; i < n; i++)
        {
            v[i] = r.nextInt(n);
            System.out.println("Elem v["+i+"] este:"+v[i]);
        }

        int sortat;
        do
            {
            sortat = 1;
            for (i = 0; i < n - 1; i++)
            {
                if (v[i] > v[i + 1])
                {
                    int aux = v[i];
                    v[i] = v[i + 1];
                    v[i + 1] = aux;
                    sortat = 0;
                }
            }
        } while (sortat == 0);

        System.out.println();
        System.out.println("Vectorul sortat este:");

        for (i = 0; i < n; i++) {
            System.out.print(" Elementul " + i + ": " + v[i]);
            System.out.println();
        }
        System.out.println();
        System.out.println("Elementul maxim este:"+v[n-1]);
    }

}
