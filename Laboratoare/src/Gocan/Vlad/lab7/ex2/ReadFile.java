package Gocan.Vlad.lab7.ex2;

import java.io.*;
import java.util.Scanner;


public class ReadFile {

    public static void main (String[] args) throws Exception {
        try{
            File myFile = new File ("C:\\Users\\Laptop\\IdeaProjects\\Laboratoare\\src\\Gocan\\Vlad\\lab7\\ex2\\data.txt");
            Scanner citire = new Scanner (myFile);
            Scanner citire1 = new Scanner (System.in);

            int count = 0;
            char s;
            System.out.println ("Dati ce caracter vreti sa-l cautati: ");
            s = citire1.next ().charAt (0);
            String l = citire.nextLine ();
            for (int i = 0; i < l.length (); i++) {
                if (l.charAt (i) == s) {
                    count++;
                }
            }
            System.out.println ("Caracterul " + s + " a aparut de " + count + " ori");}
        catch(FileNotFoundException e){
            System.out.println ("Fisier invalid");
        }



    }

}

