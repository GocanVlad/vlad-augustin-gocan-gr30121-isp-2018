package Gocan.Vlad.lab7.ex1;

public class Coffe {
    private int temp;
    private int conc;
    public static int nr = 0;


    Coffe (int t, int c) {
        temp = t;
        conc = c;
        nr++;
    }

    int getTemp () {
        return temp;
    }

    int getConc () {
        return conc;
    }

    int getNr () {
        return nr;
    }

    public String toString () {
        return "[coffe temperature=" + temp + ":concentration=" + conc + "numar=" + nr + "]";
    }}

