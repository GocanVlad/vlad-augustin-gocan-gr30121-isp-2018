package Gocan.Vlad.lab7.ex1;

public class CoffeTest {

    public static void main (String[] args) {
        CoffeMaker mk = new CoffeMaker ();
        CoffeDrinker d = new CoffeDrinker ();

        for (int i = 0; i < 15; i++) {
            Coffe c = mk.makeCoffe ();
            try {
                d.drinkCoffe (c);
            } catch (TemperatureException e) {
                System.out.println ("Exception:" + e.getMessage () + " temp=" + e.getTemp ());
            } catch (ConcentrationException e) {
                System.out.println ("Exception:" + e.getMessage () + " conc=" + e.getConc ());

            } catch (NumberException e) {
                System.out.println ("Exception:" + e.getMessage () + " num=" + e.getNr ());
                break;
            } finally {
                System.out.println ("Throw the coffe cup.\n");


            }
        }
    }
}
