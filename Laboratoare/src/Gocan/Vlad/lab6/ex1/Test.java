package Gocan.Vlad.lab6.ex1;

public class Test {

    public static void main(String[] args) {
        BankAccount ba1 = new BankAccount("Vlad",100);
        BankAccount ba2 = new BankAccount("Andrei",150);

        if(ba1.equals(ba2))
            System.out.println(ba1+" and "+ba2+ " are equals");
        else
            System.out.println(ba1+" and "+ba2+ " are NOT equals");


        System.out.println(ba1.hashCode());
        System.out.println(ba2.hashCode());
    }
}
