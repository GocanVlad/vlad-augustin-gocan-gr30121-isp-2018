package Gocan.Vlad.lab6.ex3;
public class Test {
    public static void main(String[] args) {

        Bank bank = new Bank();
        bank.addAccount("Andrei",150);
        bank.addAccount("Maria",50);
        bank.addAccount("Ioana",200);
        bank.addAccount("Mihai",100);


        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between limits");
        bank.printAccounts(0,150);
        System.out.println("Get Account by owner name");
        bank.getAccount("Andrei",900);
        System.out.println("Get All Account order by name");
        bank.getAllAccount();

    }
}